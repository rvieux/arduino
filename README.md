# Programmation de ma carte ESP8266.
Dans Arduino ID, choisir Type de carte: WEMOS D1 R1

# Correspondances
Dans Arduino ID, choisir Type de carte: WEMOS D1 R1. Utiliser le programme blink pour vérifier et le multimetre sur les sorties de la carte.

| id dans programme| numero pin sur la carte | commentaire|
|----------------|-------------------------|---------------|
|0 | D3 ||
|1 | -- |Fais planter au boot|
|2 | D4 | LED_BUILTIN dans programme blink|
|3 | RX |wtf|
|4 | D2 ||
|5 | D1 ||
|6 | -- |Fais planter au boot|
|7 | -- |Fais planter au boot|
|8 | -- |Fais planter au boot|

Il semble que ca correspond au schema suivant trouvé à l'adresse https://escapequotes.net/esp8266-wemos-d1-mini-pins-and-diagram/
![esp8266 schematics](photos/esp8266-wemos-d1-mini-pinout-757x500.png)

# Branchement cerle de LEDS WS2812B

Alimentation externe via chargeur 12v + carte d'alim pour les leds.
Chargeur USB pour l'ESP8266.
Sur la carte d'alim:
 - 5V fil rouge
 - Ground fil noir
 - Data fil vert sur D2 de l'ESP8266
 - Ground de l'ESP8266 sur ground de la carte d'alim
 - Note: dans la ref youtube ci-dessous, conseille de mettre une resistance 220 Ohms en serie sur la sortie de l'ESP8266. J'ai qu'une resistance de 3,3kOhms c'est trop

![photo branchement](photos/esp8266_et_WS2812B.jpg)

# References
How to use WS2812B with FastLEDS: https://www.youtube.com/watch?v=YgII4UYW5hU
